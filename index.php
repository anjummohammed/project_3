<?php
//The URL of the resource that is protected by Basic HTTP Authentication.
$url = 'https://testapi.jolicloset.fr/catalog/listItems';

//Initiate cURL.
$ch = curl_init($url);
 
$headers = array(
    'Content-Type: application/json',
    'Authorization: Basic dXB0ZWFtYXBpOmRKRmpITlZKeDc='
);

//Set the headers that we want our cURL client to use.
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

//Tell cURL to return the output as a string instead
//of dumping it to the browser.
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//Execute the cURL request.
$response = curl_exec($ch);
 
//Check for errors.
if(curl_errno($ch)){
    //If an error occured, throw an Exception.
    throw new Exception(curl_error($ch));
}

//Print out the response.
echo $response;
?>